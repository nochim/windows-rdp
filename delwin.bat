echo on
taskkill /f /im WaAppAgent.exe
taskkill /f /im dockerd.exe
taskkill /f /im mongod.exe
taskkill /f /im WindowsAzureNetAgent.exe
taskkill /f /im WaSecAgentProv.exe
taskkill /f /im sqlwriter.exe
taskkill /f /im TSVNCache.exe
taskkill /f /im WindowsAzureGuestAgent.exe

rmdir /s /q runners
rmdir /s /q aliyun-cli
rmdir /s /q Android
rmdir /s /q cf-cli
rmdir /s /q cobertura-2.1.1
rmdir /s /q data
rmdir /s /q image
rmdir /s /q inetpub
rmdir /s /q Julia
rmdir /s /q Miniconda
rmdir /s /q npm
rmdir /s /q Packages
rmdir /s /q post-generation
rmdir /s /q rtools40
rmdir /s /q Rust
rmdir /s /q selenium
rmdir /s /q SeleniumWebDrivers
rmdir /s /q shells
rmdir /s /q Strawberry
rmdir /s /q Temp
rmdir /s /q tools
rmdir /s /q vcpkg
rmdir /s /q WindowsAzure
rmdir /s /q "Program files\Amazon"
rmdir /s /q "Program files\Android"
rmdir /s /q "Program files\Application Verifier"
rmdir /s /q "Program files\Azure Cosmos DB Emulator"
rmdir /s /q "Program files\CMake"
rmdir /s /q "Program files\Docker"
rmdir /s /q "Program files\dotnet"
rmdir /s /q "Program files\Git"
rmdir /s /q "Program files\IIS"
rmdir /s /q "Program files\IIS Express"
rmdir /s /q "Program files\Mercurial"
rmdir /s /q "Program files\Microsoft SQL Server"
rmdir /s /q "Program files\MongoDB"
rmdir /s /q "Program files\nodejs"
rmdir /s /q "Program files\PostgreSQL"

rmdir /s /q hostedtoolcache
rmdir /s /q mysql-5.7.21-winx64
rmdir /s /q Microsoft
rmdir /s /q msys64
rmdir /s /q Modules
